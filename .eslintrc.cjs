module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  extends: ['@nuxt/eslint-config', 'prettier'],
  plugins: [],
  rules: {
    'no-console': 'off'
  }
}
