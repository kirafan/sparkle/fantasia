import vuetify from 'vite-plugin-vuetify'

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  srcDir: 'src/',
  // SPA mode, not SSG nor SSR.
  // I don't like Hydration children mismatch errors.
  ssr: false,
  spaLoadingTemplate: './loading-template.html',
  css: ['vuetify/lib/styles/main.sass'],
  build: {
    transpile: ['vuetify']
  },
  router: {
    options: {
      hashMode: true
    }
  },
  modules: [
    '@nuxtjs/device',
    '@nuxtjs/i18n',
    '@vueuse/nuxt',
    ['@pinia/nuxt', { autoImports: ['defineStore'] }],
    (_, nuxt) => {
      nuxt.hooks.hook('vite:extendConfig', (config) => {
        if (!config.plugins) {
          return
        }
        config.plugins.push(vuetify({ autoImport: true }))
      })
    }
  ],
  imports: {
    dirs: ['stores']
  },
  i18n: {
    locales: [
      {
        code: 'en',
        file: 'en-US.ts'
      },
      {
        code: 'ja',
        file: 'ja-JP.ts'
      }
    ],
    lazy: true,
    langDir: 'lang',
    defaultLocale: 'en'
  },
  typescript: {
    strict: true
  },
  devtools: { enabled: true },
  runtimeConfig: {
    public: {
      apiUrl: process.env.API_URL || 'http://localhost:8080'
    }
  },
  app: {
    head: {
      title: 'Sparkle Fantasia Official Website',
      titleTemplate: '%s - Sparkle fantasia Official Website',
      viewport: 'width=device-width, initial-scale=1, maximum-scale=1',
      charset: 'utf-8',
      link: [
        {
          rel: 'preload',
          type: 'image/png',
          href: '/ヒトデのアイコン素材.png'
        },
        { rel: 'icon', type: 'image/png', href: '/ヒトデのアイコン素材.png' }
      ],
      meta: [
        {
          hid: 'description',
          name: 'description',
          content: 'Fan-made server for discontinued kirakira game'
        },
        { name: 'robots', content: 'noindex,nofollow,noarchive' }
      ]
    }
  }
})
