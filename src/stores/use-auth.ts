import { defineStore } from 'pinia'

export const useAuth = defineStore('auth', () => {
  const isLoggedIn = ref<boolean>(false)

  const login = () => (isLoggedIn.value = true)
  const logout = () => (isLoggedIn.value = false)

  return {
    isLoggedIn,
    login,
    logout
  }
})
