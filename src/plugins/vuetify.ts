import 'vuetify/styles'
import { createVuetify, ThemeDefinition } from 'vuetify'
import { aliases, mdi } from 'vuetify/iconsets/mdi-svg'

import colors from 'vuetify/lib/util/colors'

export default defineNuxtPlugin((nuxtApp) => {
  const myCustomLightTheme: ThemeDefinition = {
    dark: false,
    colors: {
      background: '#FFF9F2',
      surface: '#FFFFFF',
      primary: colors.brown.darken1,
      secondary: '#FF6590',
      accent: '#719FFD',
      'kirakira-red': '#FF6590',
      'kirakira-blue': '#719FFD',
      'secondary-darken-1': '#018786',
      error: '#f44336',
      info: '#4caf50',
      success: '#00bcd4',
      warning: '#ff9800'
    }
  }

  const vuetify = createVuetify({
    icons: {
      defaultSet: 'mdi',
      aliases,
      sets: {
        mdi
      }
    },
    theme: {
      defaultTheme: 'myCustomLightTheme',
      themes: {
        myCustomLightTheme
      }
    }
  })
  nuxtApp.vueApp.use(vuetify)
})
